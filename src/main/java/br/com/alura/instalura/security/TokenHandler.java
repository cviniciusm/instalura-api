package br.com.alura.instalura.security;

import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public final class TokenHandler {

	private final UserDetailsService userDetailsService;
	private Key key;

	public TokenHandler(String secret, UserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;

		try {
			key = SecretKeyFactory.getInstance("PBEWithHmacSHA256AndAES_128")
					.generateSecret(new PBEKeySpec(secret.toCharArray()));
		} catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	public UserDetails parseUserFromToken(String token) {
		String username = Jwts.parser().setSigningKey(key).parseClaimsJws(token).getBody().getSubject();
		return userDetailsService.loadUserByUsername(username);
	}

	public String createTokenForUser(UserDetails user) {
		return Jwts.builder().setSubject(user.getUsername()).signWith(key, SignatureAlgorithm.HS256).compact();
	}

}