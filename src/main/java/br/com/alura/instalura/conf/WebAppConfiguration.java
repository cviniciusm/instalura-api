package br.com.alura.instalura.conf;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
public class WebAppConfiguration implements WebMvcConfigurer {

	private final RestTemplate restTemplate;
	
	WebAppConfiguration(RestTemplateBuilder builder) {
		this.restTemplate = builder.build();
	}
	
	@Override
	public void addCorsMappings(CorsRegistry registry) {

		registry
			.addMapping("/**")
			.allowedOrigins("*")
			.allowedMethods("*")
			.allowedHeaders("*")
			.allowCredentials(true);
	}
	
	@Bean RestTemplate RestTemplateBean() {
		return this.restTemplate;
	}

}