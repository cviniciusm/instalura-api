package br.com.alura.instalura.dtos.inputs;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.URL;
import org.springframework.util.Assert;

import br.com.alura.instalura.models.Usuario;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NovoUsuarioForm {

	@NotBlank
	private String login;
	
	@NotBlank
	private String senha;
	
	@NotBlank
	@URL
	private String urlPerfil;

	public boolean loginESenhaDiferentes() {
		Assert.notNull(login, "O login não deve ser nulo");
		Assert.notNull(senha, "A senha não deve ser nula");

		return !this.login.equals(senha);
	}

	public Usuario build() {
		return new Usuario(login, senha, urlPerfil);
	}

}
