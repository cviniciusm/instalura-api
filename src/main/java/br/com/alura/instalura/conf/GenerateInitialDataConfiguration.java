package br.com.alura.instalura.conf;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class GenerateInitialDataConfiguration {

	@Autowired
	private RestTemplate restTemplate;

	@Value("${server.port}")
	private String SERVER_PORT;

	public void generate() {

		URI uri = URI.create("http://localhost:" + SERVER_PORT + "/gera/dados");
		String responseBody = restTemplate.getForObject(uri, String.class);

		log.info("Gerando dados na base do instalura");
		if (!responseBody.contains("instalura"))
			log.warn("Já existem dados na base do Instalura");
		else
			log.info("Os dados foram gerados na base do instalura!");
	}
}
