package br.com.alura.instalura.security;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;




/**
 * DTO que representa as informações de login enviadas pelo cliente
 * @author alberto
 *
 */
@Getter
@Setter
public class LoginDTO {

	@NotBlank
	@Email
	private String login;
	
	@NotBlank
	private String senha;

	@Override
	public String toString() {
		return "LoginDTO [login=" + login + ", senha=" + senha + "]";
	}
	
}
