package br.com.alura.instalura.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;



@Entity
@Getter
@Setter
public class Comentario {
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Setter(value = AccessLevel.NONE)
	private Integer id;
	
	@ManyToOne
	@NotNull
	private Usuario usuario;
	
	@NotBlank
	private String texto;

	public Comentario(Usuario usuario, String texto) {
		this.usuario = usuario;
		this.texto = texto;
	}
	
}
